CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fname VARCHAR(255) NOT NULL,
  lname VARCHAR(255) NOT NULL
);

CREATE TABLE questions (
  id INTEGER PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  body VARCHAR(1000) NOT NULL,
  user_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE question_followers (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

CREATE TABLE replies (
  id INTEGER PRIMARY KEY,
  parent_id INTEGER,
  question_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  body VARCHAR(1000),
  FOREIGN KEY (parent_id) REFERENCES replies(id),
  FOREIGN KEY (question_id) REFERENCES questions(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE question_likes (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);
  
INSERT INTO
users (id, fname, lname)
VALUES
(101, 'Graham', 'Anderson'), (102, 'Morgan', 'Anderson'),
(103, 'Jonathan', 'Wilson'), (104, 'App', 'Academy');

INSERT INTO
questions (id, title, body, user_id)
VALUES
(201, 'How are you?', 'I like PAncakes', 101), (202, 'What is today', 'Tuesday', 103),
(203, 'Where are you', 'San Francisco', 103);

INSERT INTO
question_followers (id, user_id, question_id)
VALUES
(301, 102, 201), (302, 104, 203), (303, 102, 203);

INSERT INTO
replies (id, parent_id, question_id, user_id, body)
VALUES
(401, NULL, 203, 104, 'Are you sure'), (402, 401, 203, 103, 'Yes'),
(403, 401, 203, 102, 'That is correct');

INSERT INTO
question_likes (id, user_id, question_id)
VALUES
(501, 101, 202), (502, 101, 203), (503, 102, 202);