class User
  def self.find_by_id(id)
    result = QuestionsDatabase.instance.get_first_row(<<-SQL, id)
    SELECT
      *
    FROM
      users
    WHERE
      users.id = (?)
    SQL
    User.new(result)
  end
  
  def self.find_by_name(fname, lname)
    results = QuestionsDatabase.instance.execute(<<-SQL, fname, lname)
    SELECT
      *
    FROM
      users
    WHERE
      users.fname = (?)
    AND
      users.lname = (?)
    SQL
    results.map { |result| User.new(result) }
  end
  
  attr_accessor :id, :fname, :lname
  
  def initialize(opts = {})
    @id = opts['id']
    @fname = opts['fname']
    @lname = opts['lname']
  end
  
  def authored_questions 
    Question.find_by_author_id(self.id)
  end
  
  def authored_replies 
    Reply.find_by_user_id(self.id)
  end
  
  def followed_questions
    QuestionFollower.followed_questions_for_user_id(self.id)
  end
  
  def liked_questions
    QuestionLike.liked_questions_for_user_id(self.id)
  end
  
  def average_karma
    results = QuestionsDatabase.instance.get_first_row(<<-SQL, @id)
    SELECT
    CAST(COUNT(question_likes.id) AS FLOAT ) / COUNT(DISTINCT(questions.id)) as average
    FROM
    questions
    LEFT OUTER JOIN
    question_likes ON questions.id = question_likes.question_id
    WHERE questions.user_id = (?)
    SQL
    results
  end
end