class QuestionLike
  def self.find_by_id(id)
    result = QuestionsDatabase.instance.get_first_row(<<-SQL, id)
    SELECT
      *
    FROM
    question_likes
    WHERE
      question_likes.id = (?)
    SQL
    QuestionLike.new(result)
  end
  
  def self.likers_for_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT
    *
    FROM
    users
    JOIN
    question_likes ON users.id = question_likes.user_id
    WHERE
    question_likes.question_id = (?)
    SQL
    results.map { |result| User.new(result) }
  end
  
  def self.num_likes_for_question_id(question_id)
    result = QuestionsDatabase.instance.get_first_row(<<-SQL, question_id)
    SELECT
    COUNT(*)
    FROM
    question_likes
    JOIN
    questions ON question_likes.question_id = questions.id
    WHERE
    questions_likes.question_id = (?)
    SQL
    result
  end
  
  def self.liked_questions_for_user_id(user_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, user_id)
    SELECT
    *
    FROM
    questions
    JOIN
    question_likes ON questions.id = question_likes.question_id
    WHERE
    question_likes.user_id = (?)
    SQL
    results.map { |result| Question.new(result) }
  end
  
  def self.most_liked_questions(n)
    results = QuestionsDatabase.instance.execute(<<-SQL)
    SELECT
    questions.*
    FROM
    questions
    JOIN 
    question_likes ON questions.id = question_likes.question_id
    GROUP BY questions.id  
    ORDER BY COUNT(question_likes.user_id) DESC
    SQL
    
    results.map { |result| Question.new(result) }.take(n)
  end
  
  attr_accessor :id, :user_id, :question_id
  
  def initialize(opts = {})
    @id = opts['id']
    @user_id = opts['user_id']
    @question_id = opts['question_id']
  end
end