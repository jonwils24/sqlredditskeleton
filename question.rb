class Question
  def self.find_by_id(id)
    result = QuestionsDatabase.instance.get_first_row(<<-SQL, id)
    SELECT
      *
    FROM
    questions
    WHERE
      questions.id = (?)
    SQL
    Question.new(result)
  end
  
  def self.find_by_author_id(author_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, author_id)
    SELECT
      *
    FROM
    questions
    WHERE
      user_id = (?)
    SQL
    results.map { |result| Question.new(result) }
  end
  
  def self.most_followed(n)
    QuestionFollower.most_followed_questions(n)
  end
  
  def self.most_liked(n)
    QuestionLike.most_liked_questions(n)
  end
  
  attr_accessor :id, :title, :body, :user_id
  
  def initialize(opts = {})
    @id = opts['id']
    @title = opts['title']
    @body = opts['body']
    @user_id = opts['user_id']
  end
  
  def author
    User.find_by_id(self.user_id)
  end
  
  def replies
    Reply.find_by_question_id(self.id)
  end
  
  def followers
    QuestionFollower.follower_for_question_id(self.id)
  end
  
  def likers
    QuestionLike.likers_for_question_id(self.id)
  end
  
  def num_likes
    QuestionLike.num_likes_for_question_id(self.id)
  end
  
  
end