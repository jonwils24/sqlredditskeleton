class Reply
  def self.find_by_id(id)
    result = QuestionsDatabase.instance.get_first_row(<<-SQL, id)
    SELECT
      *
    FROM
    replies
    WHERE
      replies.id = (?)
    SQL
    Reply.new(result)
  end
  
  def self.find_by_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT
    *
    FROM
    replies
    WHERE
    replies.question_id = (?)
    SQL
    results.map { |result| Reply.new(result) }
  end
  
  def self.find_by_user_id(user_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, user_id)
    SELECT
      *
    FROM
      replies
    WHERE
      replies.user_id = (?)
    SQL
    results.map { |result| Reply.new(result) }
  end
  
  attr_accessor :id, :parent_id, :question_id, :user_id, :body
  
  def initialize(opts = {})
    @id = opts['id']
    @parent_id = opts['parent_id']
    @question_id = opts['question_id']
    @user_id = opts['user_id']
    @body = opts['body']
  end
  
  def author
    User.find_by_id(self.user_id)
  end
  
  def question
    Question.find_by_id(self.question_id)
  end
  
  def parent_reply
    Reply.find_by_id(self.parent_id)
  end
  
  def child_replies
    results = QuestionsDatabase.instance.execute(<<-SQL, @id)
    SELECT
      *
    FROM
      replies 
    WHERE
      parent_id = (?)  
    SQL
  end
end