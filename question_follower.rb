class QuestionFollower
  def self.find_by_id(id)
    result = QuestionsDatabase.instance.get_first_row(<<-SQL, id)
    SELECT
      *
    FROM
    question_followers
    WHERE
      question_followers.id = (?)
    SQL
    QuestionFollower.new(result)
  end
  
  def self.followers_for_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT
    *
    FROM
    users
    JOIN
    question_followers ON users.id = question_followers.user_id
    WHERE
    question_followers.question_id = (?)
    SQL
    results.map { |result| User.new(result) }
  end
  
  def self.followed_questions_for_user_id(user_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, user_id)
    SELECT
    *
    FROM
    questions
    JOIN
    question_followers ON questions.id = question_followers.question_id
    WHERE question_followers.user_id = (?)
    SQL
    results.map { |result| Question.new(result) }
  end
  
  def self.most_followed_questions(n)
    results = QuestionsDatabase.instance.execute(<<-SQL)
    SELECT
    questions.*
    FROM
    questions
    JOIN 
    question_followers ON questions.id = question_followers.question_id
    GROUP BY questions.id  
    ORDER BY COUNT(question_followers.user_id) DESC
    SQL
    
    results.map { |result| Question.new(result) }.take(n)
  end
  
  attr_accessor :id, :user_id, :question_id
  
  def initialize(opts = {})
    @id = opts['id']
    @user_id = opts['user_id']
    @question_id = opts['question_id']
  end
end